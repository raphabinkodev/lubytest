import styled from 'styled-components'

export const colors = {
    xiketic: '#03071eff',
    darkSienna: '#370617ff',
    englishViolet: '#493657ff',
    wildOrchid: '#ce7da5ff',
    celadon: '#bee5bfff',
    honeydew: '#dff3e3ff',
    apricot: '#ffd1baff',
}

export const ContainerItems = styled.div`
background-color: ${colors.honeydew};
margin: auto;
width: 100%;
height: 100vh;
display: flex;
flex-direction: column;
align-items: center; 
justify-content: center;
`

export const ContainerTodo = styled.div`
display: flex;
flex-direction: column;
align-items: center;
width: 450px;
max-width: 600px;
height: 560px;
background-color: ${colors.celadon};
border-radius: 12px;
box-shadow: 15px 10px 15px 0 rgba(155,155,155,0.3);
`

export const Formulary = styled.form`
display: flex;
align-items: center;
button {
    padding: 4px;
    margin-left: 12px;
    width: 100px;
    height: 30px;
    border: none;
    border-radius: 4px;
    background-color: ${colors.apricot};
    color: ${colors.englishViolet};
    font-weight: bold;
    text-transform: uppercase;
    transition: .3s;
    cursor: pointer;
}
button:hover {
    background-color: ${colors.englishViolet};
    color: #FFF;
    transform: scale(1.1)
}
`

export const TodoItems = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
`
export const CheckBox = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    text-align: justify;
    margin-top: 20px;

    li {
        list-style: none;
        width: 410px;
        font-size: 19px;
        font-weight: 500;
        letter-spacing: 3px;
        color: ${colors.englishViolet};
        margin-bottom: 8px;
    }
    
`
