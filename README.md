
## Modo de Execução da Aplicação

No diretório do projeto, você pode executar os seguintes scripts:

### `yarn`

Para fazer o download de todas dependências utilizadas no desenvolvimento, bastar
dar "yarn" no terminal e aguardar instalação.

### `yarn start`

Irá rodar a Aplicação em Modo Desenvolvedor.<br>
Abra [http://localhost:3000](http://localhost:3000) para ver a aplicação no Navegador.

A página será recarregada após cada edição.
