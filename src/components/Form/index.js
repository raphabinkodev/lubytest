import React, { useState, useEffect } from 'react'
import uuid from 'uuid'
import Checkbox from '../Checkbox'
import Input from '../Input'
import { Formulary, TodoItems, CheckBox } from './styles'

export default function Form() {
    const [todoList, setTodoList] = useState([])
    const [todo, setTodo] = useState('')
    
    useEffect( () => {
        if(todoList.length) return;
        const todo = JSON.parse(localStorage.getItem('todoList')) || []
        setTodoList(todo)
    }, []) //eslint-disable-line
    
    useEffect( () => {
        const formattedTodoList = JSON.stringify(todoList)
        localStorage.setItem('todoList', formattedTodoList)
    }, [todoList]);
    
    function handleNewTodo(e) {
        e.preventDefault()
        if(todo.length < 5) {
            return alert('Invalid Task!')
        }
        setTodoList([...todoList, {
            id: uuid(),
            task: todo,
            checked: false
        }])
        setTodo('')
    }
    
    function handleTodoInputChange(e) {
        setTodo(e.target.value)
    }
    
    function handleTaskSelected(id) {
        setTodoList(
            todoList.map(todo => ({
                ...todo,
                checked: todo.id === id ? !todo.checked : todo.checked
            }))
        )
    }
        
    return(
            <>
            <Formulary onSubmit={handleNewTodo}>
                <Input
                    placeholder= "Write your task"
                    type="text"
                    value={todo}
                    onChange={handleTodoInputChange}
                />
                <button type="submit">Add</button>
            </Formulary>

            <TodoItems>
                <CheckBox>
                    <ul>
                        {todoList.map((todo) => {
                            return(
                                <li key={todo.id} >
                                    <Checkbox
                                        id={todo.id}
                                        checked={todo.checked} 
                                        onChange={() => handleTaskSelected(todo.id)}
                                        title={todo.task}
                                    />
                                </li>
                            )
                        })}
                    </ul>
                </CheckBox>
            </TodoItems>
            </>
        )
}