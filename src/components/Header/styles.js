import styled from 'styled-components';
import { colors } from '../Form/styles'


export const HeaderLuby = styled.header`
h1{
    color: ${colors.englishViolet};
    text-align: center;
    letter-spacing: 0.3rem;
    font-family: Archivo;
    font-weight: 700;
    font-size: 36px;
}
p{
    color: ${colors.darkSienna};
    letter-spacing: .2rem;
    font-size: 20px;
}
`