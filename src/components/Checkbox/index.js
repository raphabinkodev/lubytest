import React from 'react';
import { Container, Label } from './styles';

function Checkbox( { checked, onChange, title, id } ) {
  return (
    <Container>
      <input 
        id={id} 
        type="checkbox" 
        checked={checked}
        onChange={onChange} 
      />
      <Label  
        htmlFor={id} 
        checked={checked}
      >
        {title}
      </Label>
    </Container>
  )
}

export default Checkbox;
