import React from 'react'
import { HeaderLuby } from './styles'

export default function Header ({title}) {
    return (
        <HeaderLuby>
            <h1>{title}</h1>
            <p>The best way to organize your tasks</p>
        </HeaderLuby>
    )
}