import React from 'react';
import Header from './components/Header'
import Form from './components/Form'
import { ContainerItems, ContainerTodo } from './components/Form/styles'


function App() {
  
  return (
      <ContainerItems>
        <ContainerTodo>
          <Header title="TodoLuby" />
          <Form />
        </ContainerTodo>
      </ContainerItems>
  );
}

export default App;
