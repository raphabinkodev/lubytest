import styled from 'styled-components';
import { colors } from '../Form/styles'

export const Container = styled.div`
    input {
    width: 260px;
    height: 22px;
    border: 0.5px solid ${colors.englishViolet};
    border-radius: 4px;
    outline: none;
    padding: 4px;
    padding-left: 10px;
    letter-spacing: 0.5px;
    }
    
`;

