import styled, {css} from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  input {
    display: flex;
    width: 20px;
    height: 20px;
    cursor: pointer;
  }
`;

export const Label = styled.label`
  display: flex;
  cursor: pointer;
  padding-left: 12px;

  ${props =>
    props.checked &&
    css`
      text-decoration: line-through;
      font-weight: bold;
    `}
`;
