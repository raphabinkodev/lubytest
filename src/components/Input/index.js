import React from 'react';

import { Container } from './styles';

function Input({ value, onChange }) {
  return (
    <Container>
      <input
        value={value}
        onChange={onChange}
      />
    </Container>
  )
}

export default Input;
